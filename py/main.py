import sys
import numpy as np
import math as m
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
import os
import time

import rk_solver
import funk as f
import pa
import globalvars as glb
import initialcond as ini
import sci_solver

# in interactive python modules may beed to be reloaded like so
#import funk as f
#from importlib import relaod
#reload(globalvars)

rang        = glb.rang
nsteps      = glb.nsteps
tf          = glb.tf
p_phi2_coeff= glb.p_phi2_coeff
shape_coeff = glb.shape_coeff
ic_style    = glb.ic_style

npoints     = glb.npoints
pa_rstop    = glb.pa_rstop
save_data   = glb.save_data
ecc0g        = glb.ecc0
p0g          = glb.potter
save_txt    = glb.save_txt

showid = glb.showid

postad = True
pa_solve = False


#--------------------------------------------------------------------------------
# Main program
# RK differential equation integrator for particle in SCHW. spacetime 
# with dissipative term from quadrupole approx. 

# we use all quantities normalized to 1/GM
# so r_s = 2*G*M is just going to be 2. Just 2. Only 2.


# Quasi-circular case:
#   2PA initial conditions + numerical solve
#   2PA initial conditions on r grid

# Elliptic case:
#   1PA initial conditions + numerical solve




# TODO :
    
    # plotting instructions for waveform 

    # Comments comments comments

    # Streamline execution for different cases 

    # change eccentricity initial conditions to be (freq., ecc.) instead of (p0, ecc.)
    
    # iteration to include RR terms in HIGHER DERIVATIVES in waveform
    
    # get back on that non circular F_r expression, SEE full_F_r




#--------------------------------------------------------------------------------
def main(p0, ecc0, 
    show_output=False, 
    direct_return_first_run=False):


    if showid == False:
        pass
    else:
      run_id = np.random.random()
      print('run  id. :: ', run_id)

    if not os.path.isdir('./outputs'):
        os.mkdir('./outputs')
    else:
        # ! clean old file
        for file in os.listdir('./outputs/'):
            os.remove('./outputs/'+file)

    #initial conditions
    # NB the values in init are [r, phi, p_r, p_phi, E]
    r_stab = r_instab = 0
    init = np.zeros(5)
    # initial conditions after PA expansion
    pa_init = np.zeros(5)



    # two version of initial conditions

    if ic_style == 'p+ec':
        #closer to what's done in literature
        init = ini.initial_ecc(p0, ecc0)
    elif ic_style == 'en+l':
        #similar to what was handy in the conservative problem
        r_stab, r_instab, init = ini.initial_with_p_phi(p_phi2_coeff)


    orbit_shape = 'elliptic'
    if shape_coeff == 1 and ecc0 == 0:
        orbit_shape = 'circular'
    elif init[4] > 1:
        orbit_shape = 'hyperbolic'
    if show_output:
        print('Initial shape is: ', orbit_shape, '\n')

    if show_output == False:
        pass
    else:
        ou = ini.outputs(init, r_stab)


    # read specification parameters for runs
    file1 = open('params.txt', 'r')
    # for every valid line, a run is going to happen
    l_count = 0
    for line in file1.readlines():
        currentl = line.rstrip()
        # ignore "commented" or empty lines
        if currentl.startswith("#") or not currentl:
            continue
        else:
            l_count += 1
            vals = currentl.split(',')
            for i in range(len(vals)):
                vals[i] = vals[i].strip()

        cut = nsteps
        sol = np.zeros((nsteps, len(init)))

        t_array = np.linspace(0, tf, nsteps, dtype=np.float32)

        # do we use PA expansion on initial conditions?
        if vals[0] == 'pa':
            next_index = 2
            if vals[1] == 'circ':
                postad, pa_init[2], pa_init[3], pa_init[4] = pa.circ_2pa(init[0], init[3])
                pa_init[0] = init[0]
                pa_init[1] = init[1]

                if show_output:
                    print('circ_PA pr = ', pa_init[2], ',  circ_PA p_phi = ', pa_init[3])


            elif vals[1] == 'elli':
                postad, pa_init[2], pa_init[3], pa_init[4] = pa.elli_2pa(p0, ecc0)
                pa_init[0] = init[0]
                pa_init[1] = init[1]
                if show_output:
                    print('elli_PA pr = ', pa_init[2], ',  elli_PA p_phi = ', pa_init[3])

        else:
            next_index = 0
            pa_init = init
            postad = False


        # solve dynamics with PA expansion on r grid
        if vals[next_index] == 'pagrid':
            pa.check_shape(orbit_shape)
            grid = np.zeros((npoints, 6))
            # initialize r_grid spanning from initial pos. until the small r that we want
            grid[:, 0] = np.linspace(init[0], pa_rstop, npoints)
            grid[0, :5] = init
            pa_solve, grid = pa.pa_solve(init, npoints, grid)

            #INTERPOLATION! INTERPOLATION!
            # to recover correct time series
            if pa_solve: 
                for i in range(len(grid[0])-2):
                    sol[:, i] = np.interp(t, grid[:, 5], grid[:, i])
            if show_output:
                print('Done with PA surrogate integration on r grid + interpolation\n')
                print('Total phase obtained : ', sol[ len(sol)-1, 1]/2/np.pi, '2*pi  rad\n')



        # solve dinamics with scipy's solve_ivp
        elif vals[next_index] == 'sci':
            infall = sci_solver.infall
            infall.terminal = True
            escape = sci_solver.escape
            escape.terminal = True
            sol = solve_ivp(sci_solver.scipy_full_ddt, [0, tf], pa_init,
                             method='DOP853', t_eval=t_array, events=(infall, escape) )
            
            if show_output:
                print('Done with run ', l_count, ', ddt integration with scipy\'s solve_ivp\n')
                print('Total phase obtained : ', sol.y[1][ len(sol.y[1])-1 ]/2/np.pi, '2*pi  rad\n')


        elif vals[next_index] == 'rk':
            if vals[next_index+1] == 'full':
                ddt = rk_solver.full_ddt
            elif vals[next_index+1] == 'tri':
                ddt = rk_solver.tri_ddt
            elif vals[next_index+1] == 'circ':
                ddt = rk_solver.circ_ddt

            sol, cut = rk_solver.rk4(ddt, pa_init, t_array, nsteps, postad, orbit_shape)
            if show_output:
                print('Done with run ', l_count, ', ddt integration with RK solver\n')
                print('Total phase obtained : ', sol[ len(sol)-1, 1]/2/np.pi, '2*pi  rad\n')

            # check whether rk terminated earlier than expected
            # because orbit fell inside horizon or seemed to escape
            # if so shorten arrays of time series
            if cut < nsteps:
                temp = sol[:cut]
                sol  = temp
                temp = t_array[:cut]
                t_array = temp
        else:
            print('Something wrong with params.txt, could not complete run ', l_count)
            print('Format for every line of params.txt is (values comma-separated):', 
                    '\'pa\'                 : optional argument to use PA expans. on IC',
                    '\'circ\' or \'elli\'   : in that case you have to say which version of PA IC you want to use',
                    '\'rk\' or \'sci\' or \'pagrid\'    : have to specify how to integrate eq. of motion',
                    '\'circ\' or \'triple\' or \'full\' : if using rk, specify which diff. eq. to use', sep=os.linesep)
        if show_output:
            print('---------------------------------------------------------------------------')

        #--------------------------------------------------------------------------------
        if save_data == False:
            print('Saving data turned off, not saving files')

        if direct_return_first_run:
            return t_array, sol


        if save_txt:
            check = input('Please confrim that you want to save .txt output',
                        '(potentially dangerous file sizes if nsteps is big)',
                        'Enter \'y\' to confirm, otherwise falling back on',
                        'more convenient .npy binary files', sep=os.linesep)
            if check != 'y':
                solfile = './outputs/sol'+str(l_count)+'.npy'
                tfile   = './outputs/t'+str(l_count)+'.npy'
                np.save(solfile, sol)
                np.save(tfile, t_array)

            else:
                solfile = './outputs/sol'+str(l_count)+'.txt'
                tfile   = './outputs/t'+str(l_count)+'.txt'
                file1 = open(solfile, 'w')
                file2 = open(tfile, 'w')
                np.savetxt(file1, sol)
                np.savetxt(file2, t_array)

        else:
            solfile = './outputs/sol'+str(l_count)+'.npy'
            tfile   = './outputs/t'+str(l_count)+'.npy'
            np.save(solfile, sol)
            np.save(tfile, t_array)

    infofile = open('./outputs/nruns.txt', 'w')
    infofile.write(str(l_count))

#--------------------------------------------------------------------------------

if __name__ == '__main__':
    main(p0g, ecc0g)