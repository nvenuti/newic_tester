import sys
import numpy as np
import math as m
import matplotlib.pyplot as plt
# from numba import jit, njit, float64, prange
import os
import time

import funk as f
import pa
import globalvars as glb
import main


nsteps      = glb.nsteps
tf          = glb.tf
save_txt    = glb.save_txt


show_output = False
direct_return_first_run = True


# compare initially given eccentricity
# and actual values computed from omega
# fluctuations

#-------------------------------------------------------------------------------------------------

def testecc(show_output=False, direct_return_first_run=False):

    s = time.time()

    if direct_return_first_run:
        nruns=1
    else:
        from pathlib import Path
        nruns = int( Path('./outputs/nruns.txt').read_text() )

    nvals = 200
    evals = np.exp( np.linspace(-25, -0.1, nvals) )
    result = np.zeros((nruns, nvals, 3))
    result[:, :, 0] = evals
    p0 = 50

    progress_update = nvals/10

    # attento che i params siano giusti
    for j in range(nvals):

        if show_output:
            print('ecc = ', evals[j])
        elif j % progress_update == 0:
            print(j, '/', nvals, ' steps')


        step = 20
        if len(sys.argv) > 1 and sys.argv[1] == 'short':
            t_array = np.zeros((nruns, nsteps//step))
            sol = np.zeros((nruns, nsteps//step, 5))
        else:
            t_array = np.zeros((nruns, nsteps))
            sol = np.zeros((nruns, nsteps, 5))


        #execute run, then load results and compute flucts.
        if direct_return_first_run:
            t_array[0], sol[0] = main.main(p0, evals[j],
                                            show_output,
                                            direct_return_first_run)

            dt = t_array[0][1]-t_array[0][0]
            initial_leg = int( (0.2+(evals[j]+(1-evals[nvals-1]))**2) *len(sol[0]))

            w_a, w_p = f.lin_interpolate_w_flucts(sol[0][:initial_leg, 1], dt, 'lowecc')
            e_w = ( m.sqrt(w_p) - m.sqrt(w_a) )/( m.sqrt(w_a) + m.sqrt(w_p) )
            # print('\n run ', 1, ' e_w = ', e_w,'\n' )

            result[0, j, 1] = e_w

            w_a, w_p = f.lin_interpolate_w_flucts(sol[0][:initial_leg, 1], dt, 'highecc')
            e_w = ( m.sqrt(w_p) - m.sqrt(w_a) )/( m.sqrt(w_a) + m.sqrt(w_p) )
            # print('\n run ', 1, ' e_w = ', e_w,'\n' )

            result[0, j, 2] = e_w

        else:
            main.main(p0, evals[j])

            for i in range(nruns):
                if save_txt == True:
                    solfile = './outputs/sol'+str(i+1)+'.txt'
                    tfile   = './outputs/t'+str(i+1)+'.txt'
                    prov_sol      = np.loadtxt(solfile)
                    prov_t_array  = np.loadtxt(tfile)
                else:
                    solfile = './outputs/sol'+str(i+1)+'.npy'
                    tfile   = './outputs/t'+str(i+1)+'.npy'
                    prov_sol      = np.load(solfile)
                    prov_t_array  = np.load(tfile)


                if len(sys.argv) > 1 and sys.argv[1] == 'short':
                    sol[i] = prov_sol[ ::step]
                    t_array[i] = prov_t_array[::step]

                else:
                    sol[i] = prov_sol
                    t_array[i] = prov_t_array

                dt = t_array[0][1]-t_array[0][0]
                initial_leg = int( (0.1+(evals[j]+(1-evals[nvals-1]))**2) *len(sol[i]))

                w_a, w_p = f.lin_interpolate_w_flucts(sol[i][:initial_leg, 1], dt, 'lowecc')
                e_w = ( m.sqrt(w_p) - m.sqrt(w_a) )/( m.sqrt(w_a) + m.sqrt(w_p) )
                # print('\n run ', i, ' e_w = ', e_w,'\n' )

                result[i, j, 1] = e_w

                w_a, w_p = f.lin_interpolate_w_flucts(sol[i][:initial_leg, 1], dt, 'highecc')
                e_w = ( m.sqrt(w_p) - m.sqrt(w_a) )/( m.sqrt(w_a) + m.sqrt(w_p) )
                # print('\n run ', i, ' e_w = ', e_w,'\n' )

                result[i, j, 2] = e_w

        if show_output:
            print('---------------------------------------------------------------------------')
            print('---------------------------------------------------------------------------')


    # file = 'result.npy'
    # # np.save(file, result)
    # oldres = np.load(file)

    fig1, (ax, bx) = plt.subplots(ncols=2, figsize=(10, 5))
    fig1.suptitle('Eccentricity with omega  1PA')
    ax.set_ylabel('$e_w$')
    ax.set_xlabel('$e_0$')
    ax.plot(result[0, :, 0], result[0, :, 1], label='1PA low ecc.')
    ax.plot(result[0, :, 0], result[0, :, 2], label='1PA high ecc')

    # ax.plot(oldres[0, :, 0], oldres[0, :, 1], label='0PA low ecc.')
    # ax.plot(oldres[0, :, 0], oldres[0, :, 2], label='OPA high ecc')

    ax.plot(result[0, :, 0], result[0, :, 0], lw=0.7, linestyle='dashed')
    
    ax.grid(True)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_title('$e_w$')
    ax.legend()


    bx.set_ylabel('')
    bx.set_xlabel('$e_0$')
    bx.plot(result[0, :, 0], (result[0, :, 1]-result[0, :, 0])/result[0, :, 0], label='1PA low ecc.')
    bx.plot(result[0, :, 0], (result[0, :, 2]-result[0, :, 0])/result[0, :, 0], label='1PA high ecc.')

    # bx.plot(oldres[0, :, 0], (oldres[0, :, 1]-oldres[0, :, 0])/oldres[0, :, 0], label='0PA low ecc.')
    # bx.plot(oldres[0, :, 0], (oldres[0, :, 2]-oldres[0, :, 0])/oldres[0, :, 0], label='0PA high ecc.')
    bx.grid(True)
    bx.set_ylim([-0.05, 0])
    bx.set_xscale('log')
    bx.set_title('relative deviation from $e_w = e_0$')
    bx.legend()


    fig1.tight_layout()
    fig1.show()

    print(time.time()-s, ' s')

    # wait with plots open
    input('Close figures and enter when done...')


#-------------------------------------------------------------------------------------------------

try:
    testecc(show_output, direct_return_first_run)
except:
    import traceback, pdb, sys
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)