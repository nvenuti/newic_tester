import sys
import numpy as np
import math as m
from numba import jit, njit, float64, prange

import funk as f
import globalvars as glb


average     = glb.average
nsteps      = glb.nsteps
tf          = glb.tf
outbound_coeff = glb.outbound_coeff

#-------------------------------------------------------------------------------------------------
# diff eq. rk_solver

@njit #(float64[:](float64[:], float64))
def circ_ddt(u, t):
    r, phi, pr, p_phi, E  = u
    res = np.zeros(5, dtype=np.float64)
    res[0] = f.dr_dt(r, pr, E)
    res[1] = f.dphi_dt(r, p_phi, E)
    res[2] = f.circ_dpr_dt(r, pr, p_phi, E)
    res[3] = f.circ_dp_phi_dt(r, p_phi, E)
    res[4] = f.circ_dE_dt(r, p_phi, E)
    return res


@njit #(float64[:](float64[:], float64))
def tri_ddt(u, t):
    r, phi, pr, p_phi, E  = u
    res = np.zeros(5, dtype=np.float64)
    res[0] = f.dr_dt(r, pr, E)
    res[1] = f.dphi_dt(r, p_phi, E)
    res[2] = f.tri_dpr_dt(r, pr, p_phi, E)
    res[3] = f.tri_dp_phi_dt(r, pr, p_phi, E)
    res[4] = f.tri_dE_dt(r, pr, p_phi, E)
    return res

@njit #(float64[:](float64[:], float64))
def full_ddt(u, t):
    r, phi, pr, p_phi, E  = u
    res = np.zeros(5, dtype=np.float64)
    res[0] = f.dr_dt(r, pr, E)
    res[1] = f.dphi_dt(r, p_phi, E)
    res[2] = f.full_dpr_dt(r, pr, p_phi, E)
    res[3] = f.full_dp_phi_dt(r, pr, p_phi, E)
    res[4] = f.full_dE_dt(r, pr, p_phi, E)
    return res




@njit
def rk4(diff, u0, t , n, postad, circular, show_output=False):


    u = np.zeros((n, len(u0)))
    u[0] = u0
    h = t[1]-t[0]

    # numba likes it when you show what's everything from the start
    k1 = np.zeros(len(u0))
    k2 = np.zeros(len(u0))
    k3 = np.zeros(len(u0))
    k4 = np.zeros(len(u0))
    
    # this first part doesn't have anything to do with RK:
    # let's see if we can back-engineer the initial pr:
    # in the quasi circular approx pr ~ sinusoid along 1 orbit
    # So take the mean along 1st oscillation and use it from then
    # variables for that:
    trigger = 0
    period = 2*np.pi/f.a(u0[0])
    old = 0
    
    progress_update = n/10

    for i in range(n-1):
    
        if show_output != False and i % progress_update == 0:
            print(i, '/', n, ' steps')

        # Instructions to compute average over 1st oscillation
        # find two consecutive maxima of pr and interpolate average trend
        # at t of second maximum
        # NB we don't want to use this after using PA expansion
        #-------------------------------------------------------------------------------------------------
        if postad == True or average == False or trigger > 3 or orbit_shape!='circular':
           pass
        else:
            # NB small oscillations can't be picked up with this method to find local maxima
            # should use second derivatives, but expensive, not worth it 
            if (
                u[i, 1] < period*(trigger+1) 
                    or    np.sign(f.dpr_dt(u[i, 0], u[i, 2], u[i, 3], u[i, 4]))
                        * np.sign(f.dpr_dt(u[i-1, 0], u[i-1, 2], u[i-1, 3], u[i-1, 4])) > 0
                ) :
                pass
            else:
                print('averaging pr oscillation at phi ', u[i, 1])
                # print(u[old, 2] - u[i, 2])

                u[i, 2] = np.mean(u[old:i, 2]) - 0.5*(u[old, 2] - u[i, 2])
                old = i
                trigger += 1
                if trigger == 1:
                    period = u[i, 1]
        #-------------------------------------------------------------------------------------------------

                    
                    

        # check whether we have reached the horizon
        if u[i, 0] <= 2.:
            print('horizon reached at t = ', h*i, ', phi = ', u[i, 1])
            return u, i
        # check whether we should stop plotting 
        # an outbound trajectory
        if u[i, 0] > outbound_coeff*u[0, 0]:
            print('particle seems to be escaping after t = ', h*i)
            return u, i

        k1 = h * diff(u[i], t[i])
        k2 = h * diff(u[i] + 0.5 * k1, t[i] + 0.5*h)
        k3 = h * diff(u[i] + 0.5 * k2, t[i] + 0.5*h)
        k4 = h * diff(u[i] + k3, t[i] + h)
        u[i+1] = u[i] + (k1 + 2*(k2 + k3 ) + k4) / 6

    return u, nsteps
