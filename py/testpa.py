import sys
import numpy as np
import math as m
import matplotlib.pyplot as plt
# from numba import jit, njit, float64, prange
import os

import funk as f
import pa
# import globalvars as glb
import initialcond as ini


# ecc0        = glb.ecc0
# p0          = glb.potter


# compare ellitpic and quasi-circ PA expansions 
# for different initial conditions

#-------------------------------------------------------------------------------------------------

def pacompare(option, order):

    if option == 'e':
        nvals = 200
        evals = np.exp( np.linspace(-25, -15, nvals) )
        # contains results of the two PA expansions
        result = np.zeros((nvals, 7))
        result[:, 0] = evals
        p0 = 50
        for i in range(nvals):
            result[i, 1:] = compare_ecc(p0, evals[i], order)

    elif option == 'p':
        nvals = 20
        pvals = np.linspace(10, 50, nvals)
        # contains results of the two PA expansions
        result = np.zeros((nvals, 7))
        result[:, 0] = pvals
        ecc0 = 0.2
        for i in range(nvals):
            result[i, 1:] = compare_p(pvals[i], ecc0, order)
    else:
        print('bad argument',
            'use \'e\' or \'p\'',
            'to choose which parameter to explore', sep=os.linesep)


    varnames = ['pi1 '+order+'PA', 'p_phi '+order+'PA', 'E '+order+'PA']
    for i in range(1, 4):
        fig1, (ax, bx) = plt.subplots(ncols=2, figsize=(10, 5))
        fig1.suptitle('Initial '+varnames[i-1])
        ax.plot(result[:, 0], abs(result[:, i]), label='ecc')
        ax.plot(result[:, 0], abs(result[:, i+3]), label='circ')
        ax.grid(True)
        ax.legend()
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel('e0')
        ax.set_ylabel('|pi1|')


        bx.plot(result[:, 0], (result[:, i+3]-result[:, i])/result[:, i+3], 
                label='(circ-ecc) / circ')
        bx.grid(True)
        bx.legend()
        bx.set_xscale('log')
        bx.set_yscale('log')
        bx.set_xlabel('e0')



        fig1.tight_layout()
        fig1.show()

    # wait with plots open
    input('Close figures and enter when done...')



# compare results of 2PA expansion at initial apastron
def compare_ecc(p0, ecc0, order):
    boolfiller, elli_pr, elli_p_phi, elli_E = pa.elli_2pa(p0, ecc0, order)
    boolfiller, circ_pr, circ_p_phi, circ_E = pa.circ_2pa(p0/(1-ecc0), p0/m.sqrt(p0-3-ecc0**2), order)
    return elli_pr, elli_p_phi, elli_E, circ_pr, circ_p_phi, circ_E

#-------------------------------------------------------------------------------------------------


if len(sys.argv) == 3:
    pacompare(sys.argv[1], sys.argv[2])
else:
    print('Use 2 argument :',
        '\'e\' or \'p\'',
        'to explore different eccentricity or semilat. rect.',
        '\'1\' or \'2\''
        'to test different PA order', sep=os.linesep)