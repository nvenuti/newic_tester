import sys
import numpy as np
import math as m
from numba import jit, njit, float64, prange


import globalvars as glb

rang = glb.rang
nu = glb.nu
compute_flucts = glb.compute_flucts
nsteps = glb.nsteps
ecc0 = glb.ecc0



#-------------------------------------------------------------------------------------------------

# inner product between two waveforms
# @njit
# def inner_prod(wf1, wf2):
#     return 0



# complex waveform from dynamics in the TT gauge
# (multiplied by d to eliminate its 1/d trend (d=distance from source) )

# NB also:
# for the moment we look at waveform in z direction 
# (simpler formulas)
@njit
def waveform(t, sol):
    r       = sol[:, 0]
    phi     = sol[:, 1]
    pr      = sol[:, 2]
    p_phi   = sol[:, 3]
    E       = sol[:, 4]

    # how should I compute the higher derivatives?
    # (for the moment using conservative eq. of motion)
    drdt = dr_dt(r, pr, E)
    drdtdt = dr2_dt2(r, pr, p_phi, E)

    dphidt = dphi_dt(r, p_phi. E) 
    dphidtdt = drphi_dt2(r, pr, p_phi, E)

    # NB dots are time derivatives

    # h+ = (..M_11 - ..M_22) /2
    h_cross = ( r*(4.*dphidt*drdt + dphidtdt*r)*np.cos(2.*phi) 
                + (drdt**2 + r*(drdtdt - 2.*dphidt**2*r))*np.sin(2.*phi) )

    # hx = ..M_12
    h_plus = ( (drdt**2 + r*(drdtdt - 2.*dphidt**2*r))*np.cos(2.*phi) 
                - r*(4.*dphidt*drdt + dphidtdt*r)*np.sin(2.*phi) )


    h = h_plus - h_cross*1j
    return

#-------------------------------------------------------------------------------------------------

# after initial condition near apastron, find first max, first min, second max
# of frequency oscillations with second derivatives
#(so it works for very small oscillations)
# @njit#(parallel=True)
def lin_interpolate_w_flucts(phi, dt, method):
    # breakpoint()

    if method == 'highecc':
        # find first max, then first min
        # and calculare w_a, w_p directly there
        # the eccentrity is big enough we can completely
        # diregard the underlying trend of the frequency
        w = (1/12   *phi[:len(phi)-4]
            - 2/3   *phi[1:len(phi)-3]
            + 2/3   *phi[3:len(phi)-1]
            -1/12   *phi[4:]
            )/ dt
        counter = 0
        cooldown = 100
        xmax = xmin = 0
        for i in range(1, len(w)-1):
            if counter > 2:
                break
            if i<cooldown or np.sign(w[i]-w[i-1]) * np.sign(w[i+1]-w[i]) > 0:
                pass
            else:
                cooldown = i + 100
                counter += 1
                if counter == 1:
                    xmax = i
                    # print('found max at t = ', i*dt)
                elif counter == 2:
                    xmin = i
                    # print('found min at t = ', i*dt)

        # import matplotlib.pyplot as plt
        # plt.title('check whether one fluct fits in $\mathtt{initial\_leg}$ :')
        # plt.plot(range(len(w)-2)*dt, (w[2:]-w[1:len(w)-1])*(w[1:len(w)-1]-w[:len(w)-2]) )
        # plt.plot(range(len(w)-1)*dt, (w[1:]-w[:len(w)-1]) )
        # plt.grid()
        # plt.show()
        # breakpoint()

        if xmax*xmin == 0:
            # print('found no local maxima of omegafluct.')
            return 1, 1

        w_a =   w[xmin]
        w_p =   w[xmax]

    elif method == 'lowecc':
        # small eccentricity requires a different method:
        # compute third derivative of phi with finite differences
        # (second derivatives of omega)
        w_deriv2 = (
                + 1./8   * phi[:len(phi)-6]
                - 1.     * phi[1:len(phi)-5] 
                + 13./8  * phi[2:len(phi)-4]
                - 13./8  * phi[4:len(phi)-2]
                + 1.     * phi[5:len(phi)-1]
                - 1./8   * phi[6:]
                ) / dt**3

            
# for pdb
# +1./8*phi[0]-c 1.*phi[1]+13./8*phi[2]- 13./8*phi[4]+ 1.*phi[5]- 1./8*phi[6]

        # (if needed) check visually whether the segment of phi data used is long enough
        # to include two maxima and not too long that we're being inefficient

        # import matplotlib.pyplot as plt
        # plt.title('check whether one fluct fits in $ \mathtt{initial\_leg}$ :')
        # plt.plot(range(len(w_deriv2))*dt, w_deriv2)
        # plt.grid()
        # plt.show()

        # plt.title('phi')
        # plt.plot(range(len(phi))*dt, phi)
        # plt.grid()
        # plt.show()
        
        # breakpoint()


        # find zeros of second derivatives
        # aka the 2 flex points between a maximum
        # NB skip firt flex point before first maximum
        counter = 0
        cooldown = 0
        flex1 = flex2 = 0
        for i in range(1, len(w_deriv2)):
            if counter > 2:
                break
            # print(w_deriv2[i],w_deriv2[i-1])
            if i<cooldown or np.sign(w_deriv2[i]) * np.sign(w_deriv2[i-1]) > 0:
                pass
            else:
                cooldown = i + 100
                counter += 1
                if counter == 2:
                    flex1 = i
                    # print('found 1st flex at t = ', i*dt)
                elif counter == 3:
                    flex2 = i
                    # print('found 2nd flex at t = ', i*dt)
        if flex1*flex2 == 0:
            print('found no flexes')
            return 1, 1

        xmax1 = int(1.5 * flex1 - 0.5 * flex2)
        xmax2 = int(1.5 * flex2 - 0.5 * flex1)

        # compute w (first derivative) at minimum (interpolated apastron)
        # and interpolate 2 maxima to get interpolated periastron at same time
        # eccentricity is small enough that we can treat the w oscillation
        # as a sinusoid
        w_a =   (1/12   *phi[int((flex1+flex2)/2)-2]
                - 2/3   *phi[int((flex1+flex2)/2)-1]
                + 2/3   *phi[int((flex1+flex2)/2)+1]
                -1/12   *phi[int((flex1+flex2)/2)+2]
                )/ dt

        w_p = (
                    (1/12   *phi[xmax1-2]
                    - 2/3   *phi[xmax1-1]
                    + 2/3   *phi[xmax1+1]
                    -1/12   *phi[xmax1+2]
                    )/dt
                    +
                    (1/12   *phi[xmax2-2]
                    - 2/3   *phi[xmax2-1]
                    + 2/3   *phi[xmax2+1]
                    -1/12   *phi[xmax2+2] 
                    )/dt
                ) /2

    # print('w_a = ', w_a, '   w_p = ', w_p)
    return w_a, w_p



#-------------------------------------------------------------------------------------------------

# compute long averages of function in linear approx.
# to then highlight fluctuations
@njit(parallel=True)
def make_mean(sol, rang):
    if compute_flucts == False:
        return [0]
    res = np.zeros((len(sol)-2*rang))
    for i in prange(rang, len(sol)-rang):
        res[i-rang] = np.mean(sol[i-rang:i+rang])
    return res


#--------------------------------------------------------------------------------------------------



# effective potential that shows expected range of orbits
def V_2(r, p_phi2):
    return 1 - 2./r + p_phi2/r**2 -2.*p_phi2/r**3

@njit(float64(float64))
def a(r):
    return 1-2./r



# RR terms for adiabatic approx.
#------------------------------------------------------------------------------------------------
@njit
def circ_dE_dt(r, p_phi, E):
   phidot = dphi_dt(r, p_phi, E)
   return - nu* 6.4* r**4 * phidot**6
 


@njit
def circ_dp_phi_dt(r, p_phi, E):
   phidot = dphi_dt(r, p_phi, E)
   return - nu* 6.4* r**4 * phidot**5


@njit
def circ_F_r(r, p_phi, E):
   return 0


@njit
def circ_dpr_dt(r, pr, p_phi, E):
    return  ( -(a(r)* (2.*(pr/r)**2 - p_phi**2/r**3) + ((p_phi/r)**2 + 1)/r**2 ) 
              / E
             + circ_F_r(r,p_phi, E)
            )

#--------------------------------------------------------------------------------------------------


# RR terms computed by hand (only first derivatives)
#--------------------------------------------------------------------------------------------------
@njit
def tri_dE_dt(r, pr, p_phi, E):
   rdot = dr_dt(r, pr, E)
   phidot = dphi_dt(r, p_phi, E)
   return - nu* ( 14.4* rdot**4 * phidot**2 + 38.4* r**2 * rdot**2 * phidot**4 + 6.4* r**4 * phidot**6 )


@njit
def tri_dp_phi_dt(r, pr, p_phi, E):
   rdot = dr_dt(r, pr, E)
   phidot = dphi_dt(r, p_phi, E)
   return - nu* ( 4.8* rdot**4 * phidot + 25.6* r**2 * rdot**2 * phidot**3 + 6.4* r**4 * phidot**5 )


@njit
def tri_F_r(r, pr, p_phi, E):
   rdot = dr_dt(r, pr, E)
   phidot = dphi_dt(r, p_phi, E)
   return - nu* ( 12.8* r**2 * rdot**2 * phidot**4 + 9.6* rdot**4 * phidot**2 )


@njit
def tri_dpr_dt(r, pr, p_phi, E):
    return  ( -(a(r)* (2.*(pr/r)**2 - p_phi**2/r**3) + (p_phi**2/r**2 + 1)/r**2 ) 
               / E
              + tri_F_r(r, pr, p_phi, E)
            )
#--------------------------------------------------------------------------------------------------

# Complete quadrupole RR computed with Mathematica
@njit
def full_dE_dt(r, pr, p_phi, E):

    return nu * (
                (-0.5333333333333333*(-2. + r)**4*(3.*p_phi**6*(9. - 5.*r)**2
                + pr**2*(-2. + r)**2*r**4*(6. - 2.*pr**2*(-2. + r) + r)**2
                + p_phi**4*(6.*r**2*(27. - 33.*r + 10.*r**2) + pr**2*(3600. - 3432.*r + 388.*r**2 + 392.*r**3 - 89.*r**4))
                + p_phi**2*r**2*(3.*(3. - 2.*r)**2*r**2 + 4.*pr**4*(-2. + r)**2*(87. - 70.*r + 14.*r**2)
                - 4.*pr**2*(-360. + 270.*r + 49.*r**2 - 73.*r**3 + 13.*r**4))))/(E**6*r**16)
                )


@njit
def full_dp_phi_dt(r, pr, p_phi, E):

    return nu * (
                (-1.6*p_phi*(-2. + r)**4*(p_phi**4*(-27. + 6.*r + 5.*r**2)
                + r**3*(2.*pr**4*(-2. + r)**3 + r*(-3. + 2.*r) - 1.*pr**2*r*(8. - 6.*r + r**2))
                + 2.*p_phi**2*r*(r*(-9. + 4.*r + r**2) + pr**2*(12. - 38.*r + 24.*r**2 - 4.*r**3))))/(E**5*r**13)
                )


# OK, SO, HERE IS THE DEAL
# THE PROBLEMATIC TERM THAT WAS COMMENTED OUT
# IS NOT PART OF Fr. IT SHOULD BE A TOTAL DERIVATIVE
#  OF A "SCHOTT" TERM OF THE ENERGY LOSS    
# LET'S HOPE THE OTHER TERMS ARE CORRECT FOR NOW
@njit
def full_F_r(r, pr, p_phi, E):

    return  nu * (
                    - 2.1333333333333333*pr**5*(-2. + r)**6 / (E**5*r**10)
                    + (1.0666666666666667*pr**3*(-2. + r)**4
                        * (-174.*p_phi**2 + 152.*p_phi**2*r - 8.*(3. + 5.*p_phi**2)*r**2 + (8. + 3.*p_phi**2)*r**3 + 2.*r**4)
                        )
                        / (E**5*r**12)

#                   - pr*(1.6*p_phi**2*(-2. + r)**4*(-1. + r)*
#                    * (27.*p_phi**4 - 24.*p_phi**4*r + p_phi**2*(18. + 5.*p_phi**2)*r**2 - 14.*p_phi**2*r**3 + (3. + 2.*p_phi**2)*r**4 - 2.*r**5))
#                    / (E**5*r**13*(2.*p_phi**2 - 1.*p_phi**2*r + 2.*r**2 + (-1. + E**2)*r**3)) - 

                    - (0.5333333333333333*pr*(-2. + r)**3
                        * (-1800.*p_phi**4 + 744.*p_phi**4*r + (-720.*p_phi**2 + 442.*p_phi**4)*r**2 + (180.*p_phi**2 - 233.*p_phi**4)*r**3
                            + 4.*(-18. + 53.*p_phi**2 + 6.*p_phi**4)*r**4 + (12. - 70.*p_phi**2)*r**5 + (10. + 3.*p_phi**2)*r**6 + r**7)
                      )
                    /(E**5*r**14)
                    )

@njit
def full_dpr_dt(r, pr, p_phi, E):
    return  ( -(a(r)* (2.*(pr/r)**2 - p_phi**2/r**3) + (p_phi**2/r**2 + 1)/r**2 ) 
              / E
             + full_F_r(r, pr, p_phi, E)
            )
#-------------------------------------------------------------------------------------------------
# from hamilton eqs.

@njit
def dr_dt(r, pr, E):
    return a(r)**2/E*pr


@njit
def dphi_dt(r, p_phi, E):
    return a(r)*p_phi/E /r**2


#-------------------------------------------------------------------------------------------------
# derived in t hamilton eqs.
# NB not including RR terms


@njit
def dr2_dt2(r, pr, p_phi, E):
    return ( (4*pr**2*a(r)**3)/(E**2*r**2) 
            + (a(r)**2
                * ( (-2*(1 + p_phi**2/r**2))/r**2 
                    + (2*p_phi**2*a(r))/r**3 
                    - (4*pr**2*a(r))/r**2) )
                /(2.*E**2)
            )


@njit 
def dphi2_dt2(r, p_phi, E):
    return (2*p_phi*pr*a(r)**2)/(en**2*r**4) - (2*p_phi*pr*a(r)**3)/(en**2*r**3)




