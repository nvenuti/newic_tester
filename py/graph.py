import sys
import numpy as np
import math as m
import matplotlib.pyplot as plt
import os


import funk as f
import globalvars as glb


rang        = glb.rang
nsteps      = glb.nsteps
tf          = glb.tf
npoints     = glb.npoints
showid      = glb.showid
coeff       = glb.coeff
save_txt    = glb.save_txt
shape_coeff = glb.shape_coeff
ecc0        = glb.ecc0



# Script to generate various types of plots.

# NB:
# Contains some pseudo-garbage as the focus shifts
# from some types of plots to others

# NNB propre plotting of some figures relies on specific run specifications
# a complete solution could be interactively selecting data to be used for
# different plots plotted but for now we're keeping things as they are

if not os.path.isdir('./outputs'):
    print('No outputs saved in ./outputs directory, run main.py')
    sys.exit()

if len(sys.argv) == 1:
        print('Run with argument :',
             '[\'short\'] (option to subsample data)',
             '[multiple] plot commands',
             '  (which are:)',
             '  polar',
             '  pr_num_vs_pa', 
             '  phi_num_vs_pa', 
             '  omega_fluct', 
             '  r_and_pr', 
             '  E_and_p_phi', 
             '  phi_t', 
             '  eff_pot', sep=os.linesep)
        sys.exit()


# for every run executed, load the relevant files, in the appropriately sized array
from pathlib import Path
nruns = int( Path('./outputs/nruns.txt').read_text() )

step = 20
if sys.argv[1] == 'short':
    t_array = np.zeros((nruns, nsteps//step))
    sol = np.zeros((nruns, nsteps//step, 5))
else:
    t_array = np.zeros((nruns, nsteps))
    sol = np.zeros((nruns, nsteps, 5))

for i in range(nruns):
    if save_txt == True:
        solfile = './outputs/sol'+str(i+1)+'.txt'
        tfile   = './outputs/t'+str(i+1)+'.txt'
        prov_sol      = np.loadtxt(solfile)
        prov_t_array  = np.loadtxt(tfile)
    else:
        solfile = './outputs/sol'+str(i+1)+'.npy'
        tfile   = './outputs/t'+str(i+1)+'.npy'
        prov_sol      = np.load(solfile)
        prov_t_array  = np.load(tfile)


    if sys.argv[1] == 'short':
        sol[i] = prov_sol[ ::step]
        t_array[i] = prov_t_array[::step]

    else:
        sol[i] = prov_sol
        t_array[i] = prov_t_array

# used to plot useful ticks in few cases
orbit_shape = 'elliptic'
if shape_coeff == 1 and ecc0 == 0:
    orbit_shape = 'circular'
# elif init[4] > 1:
#     orbit_shape = 'hyperbolic'
print('Initial shape is: ', orbit_shape, '\n')


#--------------------------------------------------------------------------------
def make_plot(sol, plot_name):

    # easy way of skipping 'short' instruction at this stage
    if plot_name == 'short':
        return None

    #--------------------------------------------------------------------------------
    # Polar graph of orbits
    elif plot_name == 'polar':

        fig1, ax = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(7, 7))
        # title = ( '(p_phi^2, E^2, r0, pr0) = ({:.3f}, {:.3f}, {:.3f}, {:.3f})'.format(init[3]**2, init[4]**2, init[0], init[2]) )
        # if showid == False:
        #     fig1.suptitle( title )
        # else:
        #     fig1.suptitle(title+'\nrun  id. :: '+str(run_id))
        for i in range(nruns):
            ax.plot(sol[i][ :, 1], sol[i][ :,0], lw=0.8)
            ax.plot(np.linspace(0, 2*np.pi, 100), [sol[i][0,0]]*100,
             label=' r(0)', color='red', linestyle='dashed', lw=0.9)

        ax.plot(np.linspace(0, 2*np.pi, 100), [2.]*100,
         color='black', lw=0.8)
        try:
            ax.plot(np.linspace(0, 2*np.pi, 100), [r_stab]*100,
             label=' r_stab', color='green', linestyle='dashed', lw=0.9)
        except:
            pass
        try:
            ax.plot(np.linspace(0, 2*np.pi, 100), [r_instab]*100,
             label=' r_instab', color='orange', linestyle='dashed', lw=0.8)
        except:
            pass

        #ax.set_rmax(1.2*np.amax(sol[:,0]))
        ax.set_rticks(np.linspace(2., 1.2*np.amax(sol[0][ :, 0]), 4))  # Less radial ticks
        ax.grid(True)
        ax.legend()

        fig1.tight_layout()
        fig1.show()


    #--------------------------------------------------------------------------------
    # compare pr(r) between numerical solve and pa solve
    elif plot_name == 'pr_num_vs_pa':

        fig1, ax3= plt.subplots()
        if showid == False:
           pass
        else:
           fig1.suptitle('run  id. :: '+str(run_id))
        ax3.set_title('pr(r)')
        ax3.plot(sol[0][ :, 0], sol[0][ :,2], lw=0.9, label='sol1')
        ax3.plot(sol[1][ :, 0], sol[1][ :,2], lw=0.9, label='sol2')

        ax3.grid(True)
        ax3.set_xlabel('r')
        ax3.legend()
        fig1.tight_layout()
        fig1.show()

    #--------------------------------------------------------------------------------
    # compare phi(t) between numerical solve and pa solve
    elif plot_name == 'phi_num_vs_pa':

        fig1, ax3= plt.subplots()
        if showid == False:
           pass
        else:
           fig1.suptitle('run  id. :: '+str(run_id))
        ax3.set_title('phi(t)')
        ax3.plot(t_array[i], sol[0][ :,1]-sol[1][:,1],
                lw=0.9, label='$\phi_{real} - \phi_{PA}$')

        ax3.set_yscale("log")
        ax3.grid(True)
        ax3.set_xlabel('t')
        ax3.legend()

        fig1.tight_layout()
        fig1.show()


    #--------------------------------------------------------------------------------
    # frequency oscillations
    elif plot_name == 'omega_fluct':
        print('Plotting omega fluctuations')


        fig1, (ax3, ax4)= plt.subplots(ncols=2)
        if showid == False:
            pass
        else:
            fig1.suptitle('run  id. :: '+str(run_id))
        ax3.set_title('$\omega(t)$')

        dt = t_array[0][1]-t_array[0][0]
        initial_leg = int(0.1*len(sol[0]))
        for i in range(nruns):
            w_a, w_p = f.lin_interpolate_w_flucts(sol[i][:initial_leg, 1], dt, 'lowecc')
            print('\nsol1  e_w = ', 
                ( m.sqrt(w_p) - m.sqrt(w_a) )/( m.sqrt(w_a) + m.sqrt(w_p) ),
                 '\n' )

            # compute omega with finite differences
            w =  (
                1/12    *sol[i][:len(sol[i])-4, 1]
                - 2/3   *sol[i][1:len(sol[i])-3, 1]
                + 2/3   *sol[i][3:len(sol[i])-1, 1]
                -1/12   *sol[i][4:, 1]
                ) / dt


            ax3.plot(t_array[i][2:len(t_array[i])-2], w,
                    lw=0.9, label='$\omega(t)$')
            ax3.plot(t_array[i][1:], (sol[i][1: ,1]-sol[i][:(len(sol[i])-1),1])/(dt), 
                    lw=0.9, label='$\omega(t)$')

        ax3.grid(True)
        ax3.set_xlabel('t')
        # ax3.legend()


        for i in range(nruns):
            y1 = sol[i][1:initial_leg ,1]
            y0 = sol[i][:initial_leg-1 ,1]
            ax4.set_title('initial fluct.')
            ax4.plot(t_array[i][1:initial_leg], (y1-y0)/(dt),
                    lw=0.9, label='$\omega(t)$')


        ax4.set_ylim([ ( 1.1*(sol[0][1,1]-sol[0][0,1]) - 0.1*np.amax(y1-y0) )/(dt),
                        (1.1*np.amax(y1-y0)-0.1*(sol[0][1,1]-sol[0][0,1])) /(dt) ])


        ax4.grid(True)

        fig1.tight_layout()
        fig1.show()


    #--------------------------------------------------------------------------------
    # radial position and momentum w respect to phi

    elif plot_name == 'r_and_pr':

        fig1, ((ax1, ax3), (ax2, ax4))= plt.subplots(nrows=2, ncols= 2, figsize=(10, 7))
        # title = ( '(p_phi^2, E^2, r0, pr0) = ({:.3f}, {:.3f}, {:.3f}, {:.3f})'.format(init[3]**2, init[4]**2, init[0], init[2]) )
        # if showid == False:
        #     fig1.suptitle( title )
        # else:
        #     fig1.suptitle(title+'\nrun  id. :: '+str(run_id))

        ax1.set_title('r($\phi$)')
        for i in range(nruns):
            ax1.plot(sol[i][ :, 1], sol[i][ :,0], lw=0.9)


        try:
            ax1.plot([sol[i][ 0, 1], shortsol[len(sol[i])-1, 1]], [r_instab]*2,
                     lw=0.9, label=' r_instab', linestyle='dashed')
        except:
            pass
        ax1.grid(True)
        ax1.set_xlabel('$\phi$')
        # ax1.set_xscale("log")


        ax2.set_title('pr($\phi$)')
        for i in range(nruns):
            ax2.plot(sol[i][ :, 1], sol[i][ :,0], lw=0.9)

        if orbit_shape == 'elliptic':
            ax2.set_ylim([-2*np.amax(sol[0][:, 2]), np.amax(sol[0][:, 2])])
        elif orbit_shape == 'circular':
            try:
                ax2.set_ylim([sol[0][ int(len(t1)*.95), 2]*3, 0.0000001])
            except:
                pass
        ax2.grid(True)
        ax2.set_xlabel('$\phi$')


        ax3.set_title('initial flutcs. of r($\phi$)')

        for i in range(nruns):
            r_mean = f.make_mean(sol[i][ :int(len(t_array[i])*coeff), 0], rang)
            if len(r_mean) != 1:
                ax3.plot(sol[i][ rang:int(len(t_array[i])*coeff)-rang, 1],
                         sol[i][ rang:int(len(t_array[i])*coeff)-rang, 0] - r_mean,
                         lw=0.9)

        ax3.grid(True)


        ax4.set_title('initial flucts. of pr($\phi$)')

        for i in range(nruns):
            pr_mean = f.make_mean(sol[i][ :int(len(t_array[i])*coeff), 2], rang)
            if len(pr_mean) != 1:
                ax4.plot(sol[i][ rang:int(len(t_array[i])*coeff)-rang, 1],
                         sol[i][ rang:int(len(t_array[i])*coeff)-rang, 2] - pr_mean,
                        lw=0.9)

        ax4.grid(True)

        fig1.legend()

        fig1.tight_layout()
        fig1.show()



    #--------------------------------------------------------------------------------
    # Loss of Energy and p_phi
    elif plot_name == 'E_and_L':

        fig1, (ax1, ax2)= plt.subplots(nrows=1, ncols= 2, figsize=(10, 5))
        # title = ( '(p_phi^2, E^2, r0, pr0) = ({:.3f}, {:.3f}, {:.3f}, {:.3f})'.format(init[3]**2, init[4]**2, init[0], init[2]) )
        # if showid == False:
        #     fig1.suptitle( title )
        # else:
        #     fig1.suptitle(title+'\nrun  id. :: '+str(run_id))

        ax1.set_title('E(t)')
        for i in range(nruns):
            ax1.plot(t_array[i], sol[i][ :,4])
        # ax1.plot(t_array[i], sol[i][ :,4], linestyle='dashed')

        ax1.grid(True)
        ax1.set_xlabel('t')

        ax2.set_title('p_phi(t)')
        for i in range(nruns):
            ax2.plot(t_array[i], sol[i][ :,3])
        # ax2.plot(t_array[i], sol[i][ :,3], linestyle='dashed')

        ax2.grid(True)
        ax2.set_xlabel('t')

        fig1.tight_layout()
        fig1.show()


    #--------------------------------------------------------------------------------
    # Phase phi(t))
    elif plot_name == 'phi_t':

        fig1, (ax1, ax2)= plt.subplots(nrows=1, ncols= 2, figsize=(10, 5))
        fig1.suptitle('run  id. :: '+str(run_id))
        ax1.set_title('$\phi$(t)')
        for i in range(nruns):
            ax1.plot(t, sol[i][:,1])
        ax1.grid(True)
        ax2.set_title('d$\phi$/d$t$')
        # ax2.plot(t, sol[:,3])
        ax2.grid(True)
        fig1.tight_layout()
        fig1.show()

    #--------------------------------------------------------------------------------
    # Effective potential
    elif plot_name == 'eff_pot':

        x = np.linspace(2., sol[i][0,0], 1000)
        y = np.zeros(1000)
        y[:] = f.V_2(x[:], sol[i][0, 3]**2)
        fig1, ax3= plt.subplots()
        if showid == False:
            pass
        else:
            fig1.suptitle('run  id. :: '+str(run_id))
        ax3.set_title('f.V_2(r)')
        ax3.plot(x, y)
        ax3.axvline(x=sol[i][0,0], label='r(0)', color='red')
        ax3.plot([0, x[999]], [sol[i][0, 4], sol[i][0, 4]])
        try:
            ax3.axvline(x=r_stab, label='r_stab', color='green', linestyle='dashed')
        except:
            pass
        try:
            ax3.axvline(x=r_instab, label='r_instab', color='orange', linestyle='dashed')
        except:
            pass
        # ax.scatter(r_stab, V(r_stab), s=5,  color='red')
        ax3.set_xlim([0, x[999]])
        try:
            ax3.set_ylim([0.9*f.V_2(r_stab), f.V_2(r_instab)*1.05])
        except:
            pass
        ax3.plot([0, x[999]], 2*[sol[i][0,4]**2])
        ax3.grid(True)
        ax3.set_xlabel('r')
        fig1.tight_layout()
        fig1.show()

    #--------------------------------------------------------------------------------

    else:
        print(plot_name, ' doesn\'t match any plotting command')
        print('plotting commands are:')
        print('polar',
             'pr_num_vs_pa', 
             'phi_num_vs_pa', 
             'omega_fluct', 
             'r_and_pr', 
             'E_and_p_phi', 
             'phi_t', 
             'eff_pot', sep=os.linesep)

    return 0


# --------------------------------------------------------------------------------


for i in range(1, len(sys.argv)):
    make_plot(sol, sys.argv[i])

# wait with plots open
input('Close figures and enter when done...')
