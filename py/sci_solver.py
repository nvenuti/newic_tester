import sys
import numpy as np
import math as m

import funk as f
import globalvars as glb


nu          = glb.nu
p0          = glb.potter
e0          = glb.ecc0
ic_style    = glb.ic_style
shape_coeff = glb.shape_coeff
p_phi2_coeff= glb.p_phi2_coeff
outbound_coeff = glb.outbound_coeff

# functions to integration with scipy-implemented solver
# just like the standard ones but un-jitted

# REMARKS AFTER TRIAL:
# 1) DOP853 method much more efficient and acurate than RK45 (expected)
# 2) scipy is faster only for short integration time spans
# 3) generally unreliable total phase results


def scipy_full_ddt(t, u):
    r, phi, pr, p_phi, E  = u
    res = np.zeros(5, dtype=np.float64)
    res[0] = scipy_dr_dt(r, pr, E)
    res[1] = scipy_dphi_dt(r, p_phi, E)
    res[2] = scipy_full_dpr_dt(r, pr, p_phi, E)
    res[3] = scipy_full_dp_phi_dt(r, pr, p_phi, E)
    res[4] = scipy_full_dE_dt(r, pr, p_phi, E)
    return res



#--------------------------------------------------------------------------------------------------

# Complete quadrupole RR computed with Mathematica

def scipy_full_dE_dt(r, pr, p_phi, E):

    return nu * (
                (-0.5333333333333333*(-2. + r)**4*(3.*p_phi**6*(9. - 5.*r)**2
                + pr**2*(-2. + r)**2*r**4*(6. - 2.*pr**2*(-2. + r) + r)**2
                + p_phi**4*(6.*r**2*(27. - 33.*r + 10.*r**2) + pr**2*(3600. - 3432.*r + 388.*r**2 + 392.*r**3 - 89.*r**4))
                + p_phi**2*r**2*(3.*(3. - 2.*r)**2*r**2 + 4.*pr**4*(-2. + r)**2*(87. - 70.*r + 14.*r**2)
                - 4.*pr**2*(-360. + 270.*r + 49.*r**2 - 73.*r**3 + 13.*r**4))))/(E**6*r**16)
                )


def scipy_full_dp_phi_dt(r, pr, p_phi, E):

    return nu * (
                (-1.6*p_phi*(-2. + r)**4*(p_phi**4*(-27. + 6.*r + 5.*r**2)
                + r**3*(2.*pr**4*(-2. + r)**3 + r*(-3. + 2.*r) - 1.*pr**2*r*(8. - 6.*r + r**2))
                + 2.*p_phi**2*r*(r*(-9. + 4.*r + r**2) + pr**2*(12. - 38.*r + 24.*r**2 - 4.*r**3))))/(E**5*r**13)
                )


# OK, SO, HERE IS THE DEAL
# THE PROBLEMATIC TERM THAT WAS COMMENTED OUT
# IS NOT PART OF Fr. IT SHOULD BE A TOTAL DERIVATIVE
#  OF A "SCHOTT" TERM OF THE ENERGY LOSS    
# LET'S HOPE THE OTHER TERMS ARE CORRECT FOR NOW

def scipy_full_F_r(r, pr, p_phi, E):

    return  nu * (
                    - 2.1333333333333333*pr**5*(-2. + r)**6 / (E**5*r**10)
                    + (1.0666666666666667*pr**3*(-2. + r)**4
                        * (-174.*p_phi**2 + 152.*p_phi**2*r - 8.*(3. + 5.*p_phi**2)*r**2 + (8. + 3.*p_phi**2)*r**3 + 2.*r**4)
                        )
                        / (E**5*r**12)

#                   - pr*(1.6*p_phi**2*(-2. + r)**4*(-1. + r)*
#                    * (27.*p_phi**4 - 24.*p_phi**4*r + p_phi**2*(18. + 5.*p_phi**2)*r**2 - 14.*p_phi**2*r**3 + (3. + 2.*p_phi**2)*r**4 - 2.*r**5))
#                    / (E**5*r**13*(2.*p_phi**2 - 1.*p_phi**2*r + 2.*r**2 + (-1. + E**2)*r**3)) - 

                    - (0.5333333333333333*pr*(-2. + r)**3
                        * (-1800.*p_phi**4 + 744.*p_phi**4*r + (-720.*p_phi**2 + 442.*p_phi**4)*r**2 + (180.*p_phi**2 - 233.*p_phi**4)*r**3
                            + 4.*(-18. + 53.*p_phi**2 + 6.*p_phi**4)*r**4 + (12. - 70.*p_phi**2)*r**5 + (10. + 3.*p_phi**2)*r**6 + r**7)
                      )
                    /(E**5*r**14)
                    )


def scipy_full_dpr_dt(r, pr, p_phi, E):
    return  ( -(f.a(r)* (2.*(pr/r)**2 - p_phi**2/r**3) + (p_phi**2/r**2 + 1)/r**2 ) 
              / E
             + scipy_full_F_r(r, pr, p_phi, E)
            )
#-------------------------------------------------------------------------------------------------



def scipy_dr_dt(r, pr, E):
    return f.a(r)**2/E*pr



def scipy_dphi_dt(r, p_phi, E):
    return f.a(r)*p_phi/E /r**2



#-------------------------------------------------------------------------------------------------

# stop integration if we reach the horizon
def infall(t, y):
    return y[0]-2.

# stop integration if particle seems to be escaping
# (aka radius much bigger than initial value)
def escape(t, y):
    if ic_style == 'p+ec':
        return y[0] - p0/(1-e0) * outbound_coeff
    if ic_style == 'ec+l':
        return  ( p_phi2_coeff*2. * (1 + m.sqrt(1-3/p_phi2_coeff) )
                * shape_coeff
                * outbound_coeff )
