import sys
import numpy as np
import math as m

import globalvars as glb
from funk import a, V_2

nu = glb.nu
nsteps = glb.nsteps
tf = glb.tf
shape_coeff = glb.shape_coeff
ecc0 = glb.ecc0
ic_style = glb.ic_style

#-------------------------------------------------------------------------------------------------



def outputs(init, r_stab):

    if init[4]**2 < V_2(init[0], init[3]**2)-2*np.finfo(np.float64).eps:
        print('bad initial energy')
        sys.exit()

    print('nu       = ', nu)
    print('p_phi    = ', init[3])
    print('E^2      = ', init[4]**2)

    if ic_style == 'en+l' and r_stab != 0:
        print('r0       = ', shape_coeff, ' * r_stab')
    else:
        print('r0       = ', init[0])
    if ic_style == 'p+ec':
        print('ecc0     = ', ecc0)
    print('')
    print('so, init [r, phi, p_r, p_phi, E] is :\n', init, '\n')
    print('tf       = ', tf)
    print('nsteps   = ', nsteps)
    
    if init[4]**2 > 1 :
        print('E>1, orbita dovrebbe essere iperbolica')
    print('---------------------------------------------------------------------------')
    print('')
    return 0


# elliptic iniitial conditions at apastron 
# using semilatus rectum and eccentricity
def initial_ecc(p0, ecc0):

#    #semi axis
#    a0 = rp0 / (1-ecc0^2)
#    
#    #initial position of reference is periastron
#    r0 = p0 / (1-ecc0)

    # initial position ar apastron
    r0 = p0 / (1-ecc0)
    
    #adiabatic angular momentum
    j0 = p0 / m.sqrt(p0 - 3 - ecc0**2)
    
    #energy at periastron
    E = m.sqrt( a(r0) * ((j0/r0)**2 +1) )
    
    return np.array([r0, 0.,  0, j0, E])





# initial conditions at apastron/periastron depending on shape_coeff >1 / <1
# determine the other quantities from fixed initial p_phi and radius
def initial_with_p_phi(p_phi2_coeff):
# NB sqrt INTRODUCES ERROR, SO FOR THE MARGINAL CASE IT'S BETTER TO
# FIX p_phi^2 AND FROM THAT GET p_phi.
# SO WE DO THAT

    p_phi2 = p_phi2_coeff * 2. * 2.
    p_phi = m.sqrt(p_phi2)


    if p_phi2 == 12.:
        print('last stable orbit\n')
        r_stab = p_phi2/2.
        r_instab = r_stab

        r0 = r_stab
        E2 = V_2(r0, p_phi2)
        E = m.sqrt(E2)

        init = np.array([r0, 0.,  -((E2-V_2(r0, p_phi2))**0.5/a(r0)), p_phi, E])

    elif p_phi2 < 12.:
        print('no stable orbit, check which initial vals to use\n')
        r_stab = r_instab = None
        r0 = 2. * 5
        E2 = V_2(r0, p_phi2)
        E = m.sqrt(E2)

        init = np.array([r0, 0.,  -((E2-V_2(r0, p_phi2))**0.5/a(r0)), p_phi, E])

    else:
        print('stable orbits available\n')
        r_stab   = p_phi2/2. * (1 + m.sqrt(1-3/p_phi2_coeff) )
        r_instab = p_phi2/2. * (1 - m.sqrt(1-3/p_phi2_coeff) )

        r0 = r_stab * shape_coeff
        E2 = V_2(r0, p_phi2)
        E = m.sqrt(E2)
        # E = 1.0005

        # NB if you remove the minus in the pr initial value
        # the trigger for escaping particles in rk4 might activate unintendendly early
        init = np.array([r0, 0.,  -((E2-V_2(r0, p_phi2))**0.5/a(r0)), p_phi, E])


    return r_stab, r_instab, init


