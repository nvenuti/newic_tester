import sys
import numpy as np
import math as m
import matplotlib.pyplot as plt
from numba import jit, njit, float64
import time

# import warnings
# warnings.simplefilter("error")
# warnings.filterwarnings('error', category=DeprecationWarning)



# rk differential equation integrator for particle in SCHW. spacetime 

# ALL DONE WITH THIS
# NO UPDATES SINCE 10/02/23

#constants
# like we had all quantities normalized to 1/GM
G = 1
M = 1
# so r_s is just going to be 2. Just 2. Only 2.
# r_s = 2*G*M

# easier to define them here so the funcs see them
L = 1.
L2 = 1.
E2 = 1.
E = 1.

nsteps = 400000
tf     = nsteps * 0.2

# do you want to show the run identifier on the figures?
showid = False

#--------------------------------------------------------------------------------


start = time.time()

# effective potential that shows expected range of orbits
@njit
def V_2(r):
    return 1. - 2./r + L2/r**2 -2.*L2/r**3

# newtonian stuff
@njit
def newt_V_2(r):
    return 1. - 2./r + L2/r**2

@njit
def newt_dr_dt(r, pr):
    return pr/E

@njit
def newt_dphi_dt(r):
    return L/E /r**2

@njit
def newt_dpr_dt(r, pr):
    return - (1./r**2 - L**2/r**3 )/ E

#--------------------------------------------------------------------------------



# actual GR gang
@njit
def a(r):
    return 1-2./r

@njit
def dr_dt(r, pr):
    return a(r)**2./E*pr


@njit
def dphi_dt(r):
    return a(r)*L/E /r**2


@njit
def dpr_dt(r, pr):
    return (
            -( a(r)* (2.*(pr/r)**2 - L**2/r**3) + (L**2/r**2 + 1)/r**2 ) 
             / E
           )

#--------------------------------------------------------------------------------
# diff eq. rk_solver


@njit #(float64[:](float64[:], float64))
def ddt(u, t):
    r, phi, pr = u
    res = np.zeros(3, dtype=np.float64)
    res[0] = dr_dt(r, pr)
    res[1] = dphi_dt(r)
    res[2] = dpr_dt(r, pr)
    return res

@njit #(float64[:](float64[:], float64))
def newt_ddt(u, t):
    r, phi, pr = u
    res = np.zeros(3, dtype=np.float64)
    res[0] = newt_dr_dt(r, pr)
    res[1] = newt_dphi_dt(r)
    res[2] = newt_dpr_dt(r, pr)
    return res


@njit
def rk4(f, u0, t , n):
    
    u = np.zeros((n, len(u0)))
    u[0] = u0
    h = t[1]-t[0]

    # numba likes it when you show what's everything from the start
    k1 = np.zeros(len(u0))
    k2 = np.zeros(len(u0))
    k3 = np.zeros(len(u0))
    k4 = np.zeros(len(u0))

    for i in range(n-1):
        # check whether we have reached the horizon
        if u[i, 0] <= 2.:
            print('horizon reached at t = ', h*i)
            return u, i
        # check whether we should stop plotting 
        # an outbound trajectory
        if u[i, 0] > 500*u[0, 0]:
            print('particle seems to be escaping after t = ', h*i)
            return u, i

        k1 = h * f(u[i], t[i])    
        k2 = h * f(u[i] + 0.5 * k1, t[i] + 0.5*h)
        k3 = h * f(u[i] + 0.5 * k2, t[i] + 0.5*h)
        k4 = h * f(u[i] + k3, t[i] + h)
        u[i+1] = u[i] + (k1 + 2*(k2 + k3 ) + k4) / 6

    return u, nsteps



#--------------------------------------------------------------------------------
if showid == False:
    pass
else:
    run_id = np.random.random()
    print('run  id. :: ', run_id)




#initial conditions

# first fix L, then other stuff consequently
# values in init are [r, phi , p_r]

# NB SQRT INTRODUCES ERROR, SO FOR THE MARGINAL CASE IT'S BETTER TO
# FIX L^2 AND DERIVE L.
# SO WE DO THAT

l_coeff = 3.*(1 + 0.2)

L2 = l_coeff * 2. * 2.
L = m.sqrt(L2)

# l_coeff = m.sqrt(3*(1 + 0))

# L = m.sqrt(l_coeff) * 2.
# L2 = L*L


if L2 == 12*G**2*M**2:
    print('last stable orbit')
    r_stab = L2/2.
    r_instab = r_stab

    r0 = r_stab
    E2 = V_2(r0)
    E = m.sqrt(E2)

    init = np.array([ r0, 0., -((E2-V_2(r0))**0.5/a(r0)) ], dtype=np.float64)

elif L2 < 12*G**2*M**2:
    print('no stable orbit')
    r0 = 2. * 3
    E2 = V_2(r0)
    E = m.sqrt(E2)
    init = np.array([ r0, 0., -((E2-V_2(r0))**0.5/a(r0)) ], dtype=np.float64)

else:
    print('stable orbits available')
    r_stab   = L2/2. * (1 + m.sqrt(1-3/l_coeff) )
    r_instab = L2/2. * (1 - m.sqrt(1-3/l_coeff) )

    r0 = r_stab*1.5
    E2 = V_2(r0) #*(1-np.finfo(np.float64).eps)
    #*(1-np.finfo(np.float64).eps)
    E = m.sqrt(E2)

    # NB if you remove the minus in the pr initial value
    # the trigger for escaping particles in rk4 might activate unintendendly early
    init = np.array([ r0, 0., -((E2-V_2(r0))**0.5/a(r0)) ], dtype=np.float64)


#--------------------------------------------------------------------------------


print('L   = ', m.sqrt(l_coeff), ' * r_s')
try:
    print('r0  = ', r0/r_stab, ' * r_stab')
except:
    pass

if E2 < V_2(r0):
    print('bad initial energy (< V_2(r0) )')
    sys.exit()


print('E^2 = ', E2)
print('init [r, phi, p_r] =', init)
print('tf     = ', tf)
print('nsteps = ', nsteps)

if E2 > 1 :
    print('orbita dovrebbe essere iperbolica')

t = np.linspace(0, tf, nsteps)

cut1 = cut2 = nsteps

# solving ODE
sol = np.zeros((2, nsteps, len(init)))
sol[0], cut1 = rk4(ddt, init, t, nsteps)
sol[1], cut2 = rk4(newt_ddt, init, t, nsteps)

cut = min(cut1, cut2)

# check whether rk terminated earlier than expected
# because orbit fell inside horizon or seemed to escape
# if so shorten time array and newtonian solution
if cut >  0:
    temp = sol[:, :cut]
    sol = temp
    temp = t[:cut]
    t = temp



#--------------------------------------------------------------------------------



#plots

shortsol=sol
shortt=t

# shortsol = np.zeros((2, len(sol[0, ::100]), len(init)))
# shortsol[0] = sol[0, ::100]
# shortsol[1] = sol[1, ::100]
# shortt = t[::100]

#Polar graph of orbits

fig1, ax = plt.subplots(subplot_kw={'projection': 'polar'})
title = ( '(L^2, E^2, r0, pr0) = ({:.3f}, {:.3f}, {:.3f}, {:.3f})'.format(L2, E2, r0, init[2]) )
if showid == False:
    fig1.suptitle( title )
else:
    fig1.suptitle(title+'\nrun  id. :: '+str(run_id))
ax.plot(shortsol[0, :, 1], shortsol[0, :,0], lw=0.8)
ax.plot(shortsol[1, :, 1], shortsol[1, :,0], lw=0.8)
# per far vedere che la precessione ci becca, insomma
# ax.plot([0, 0, 1.0969480372679585], [r0, 0, r0], color='red')
ax.plot(np.linspace(0, 2*np.pi, 100), [2.]*100, color='black', lw=0.8)
ax.plot(np.linspace(0, 2*np.pi, 100), [init[0]]*100, label='r(0)', color='red', linestyle='dashed', lw=0.9)
try:
    ax.plot(np.linspace(0, 2*np.pi, 100), [r_stab]*100, label='r_stab', color='green', linestyle='dashed', lw=0.9)
except:
    pass
try:
    ax.plot(np.linspace(0, 2*np.pi, 100), [r_instab]*100, label='r_instab', color='orange', linestyle='dashed', lw=0.8)
except:
    pass
# ax.set_rmax(1.2*np.amax(shortsol[:,0]))
ax.set_rticks(np.linspace(2., 1.2*np.amax(shortsol[0, :,0]), 4))  # Less radial ticks
ax.grid(True)
ax.legend(loc='upper right')


# Radial quantities along the motion

# fig2, (ax1, ax2)= plt.subplots(nrows=1, ncols= 2, figsize=(8, 5))
# title = ( '(L^2, E^2, r0, pr0) = ({:.3f}, {:.3f}, {:.3f}, {:.3f})'.format(L2, E2, r0, init[2]) )
# if showid == False:
#     fig2.suptitle( title )
# else:
#     fig2.suptitle(title+'\nrun  id. :: '+str(run_id))
# ax1.set_title('r($\phi$)')
# ax1.plot(shortsol[0, :, 1], shortsol[0, :,0])
# # ax1.plot(shortsol[1, :, 1], shortsol[1, :,0])

# try:
#     ax1.plot([shortsol[0, 0, 1], shortsol[0, len(shortsol[0])-1, 1]], [r_instab]*2, lw=0.9, label='r_instab', linestyle='dashed')
# except:
#     pass
# ax1.grid(True)
# ax1.set_xlabel('$\phi$')
# # ax1.set_xscale("log")
# ax1.legend(loc='upper right')

# ax2.set_title('pr($\phi$)')
# ax2.plot(shortsol[0, :, 1], shortsol[0, :,2])
# # ax2.plot(shortsol[1, :, 1], shortsol[1, :,2])
# # ax2.set_ylim([shortsol[0, int(len(shortsol[0])*0.75)-1, 2], 0])

# ax2.grid(True)
# ax2.set_xlabel('$\phi$')



# Energy conservation

# fig4, ax1= plt.subplots()
# title = ( '(L^2, E^2, r0, pr0) = ({:.3f}, {:.3f}, {:.3f}, {:.3f})'.format(L2, E2, r0, init[2]) )
# if showid == False:
#     fig4.suptitle( title )
# else:
#     fig4.suptitle(title+'\nrun  id. :: '+str(run_id))

# ax1.set_title('$E^2$(# steps/100) - $E^2$(0)')

# x = range(len(shortsol[0]))
# y = (shortsol[0, :,2]**2*a(shortsol[0, :, 0])**2 + a(shortsol[0, :, 0])*(L2/ shortsol[0, :, 0]**2 +1)) - (shortsol[0, 0,2]**2*a(shortsol[0, 0, 0])**2 + a(shortsol[0, 0, 0])*(L2/ shortsol[0, 0, 0]**2 +1)) 
# ax1.plot(x, y)
# # ny = (shortsol[1, :,2]**2 + (L2/shortsol[1, :, 0]**2 - 2/shortsol[1, :, 0])) - (shortsol[1, 0,2]**2 + (L2/ shortsol[1, 0, 0]**2 -2/shortsol[1, 0, 0]))
# # ax1.plot(x, ny) 

# ax1.set_ylim( [ -10* y[int(len(y)/2)],  10*y[int(len(y)/2)] ] )
# ax1.grid(True)



try:
    x = np.linspace(2., 3*r_stab, 1000)
except:
    x = np.linspace(2., 1.05*r0, 1000)
y = np.zeros(1000)
fig3, ax3= plt.subplots()
if showid == False:
    pass
else:
    fig3.suptitle('run  id. :: '+str(run_id))
ax3.set_title('$\~V(r)$')

y[:] = V_2(x[:])
ax3.plot(x, y)
# y[:] = newt_V_2(x[:])
# ax3.plot(x, y)
ax3.axvline(x=init[0], label='r(0)', color='red')
try:
    ax3.axvline(x=r_stab, label='r_stab', color='green', linestyle='dashed')
except:
    pass
try:
    ax3.axvline(x=r_instab, label='r_instab', color='orange', linestyle='dashed')
except:
    pass
# ax.scatter(r_stab, V(r_stab), s=5,  color='red')
ax3.set_xlim([0, x[999]])
try:
    ax3.set_ylim([0.9*V_2(r_stab), V_2(r_instab)*1.1])
except:
    pass
ax3.plot([0, x[999]], [E**2]*2, label='$E^2(0)$')
ax3.grid(True)
ax3.legend(loc='upper right')
ax3.set_xlabel('r')



fig1.tight_layout()
# fig2.tight_layout()
fig3.tight_layout()
# fig4.tight_layout()

fig1.show()
# fig2.show()
fig3.show()
# fig4.show()


print('angolo totale percorso : ', sol[0, len(sol[0])-1, 1]/2/np.pi, '2*pi  rad')

end = time.time()
print(end-start, ' s')



# figg, axx = plt.subplots()
# figg.suptitle('total $\phi$ before artificial infall')
# axx.scatter([1, 0.5, 0.2, 0.15, 0.121, 0.1208, 0.1209, 0.1205, 0.12, 0.11, 0.1, 0.08, 0.05], [76, 85, 100, 105, 911, 1500, 8061, 35562, 352940, 323830, 294737, 242179, 60067], lw=1, marker='o')
# axx.set_xscale("log")
# axx.set_xticks([1, 0.5, 0.2, 0.12, 0.05])
# axx.set_yscale("log")
# axx.set_xlabel('$\Delta t$')
# axx.grid(True)
# figg.show()