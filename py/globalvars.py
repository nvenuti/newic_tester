# parameters that it's easier to just share among other files

# in the test mass limit nu -> mass ratio
nu = 1e-2

#---------------------------------------------------------------------------------
# initial conditions

# coefficient of L^2 with respect to r_s^2
# for initial conditions with L
# NB sqrt INTRODUCES ERROR, SO FOR THE MARGINAL CASE IT'S BETTER TO
# FIX L^2 AND DERIVE L.
# SO WE DO THAT
# (coefficient formatted 
p_phi2_coeff = 3 * (1 + 5)

# coefficient of r0 with respect to r_stab
# for initial conditions with L
shape_coeff = 1

# which definition of initial conditions:
# 'p+ec' = semilatus rect. + ecc.
# 'en+l' = energy + angular momentum
ic_style = 'p+ec'

#keplerian eccentricity for initial conditions
ecc0 = 1e-8
# semilatus rectum gud now. I use
potter = 50
#---------------------------------------------------------------------------------

nsteps = 20000
tf     = 100000

npoints = 20000
pa_rstop = 6.1

# lenght of mean on flucts
# has to be checked by hand...
rang   = 2000
compute_flucts = True

#---------------------------------------------------------------------------------

# do we want to make better initial conditions with averages?
average = False

# how far do we want to follow a trajectory compared to value of initial radius
outbound_coeff = 5

save_data = True

save_txt = False

#---------------------------------------------------------------------------------

#Plotting-related variables that are easier to find if here

# till what point do we want to look at flucts of r, pr? (beginning is 0, end is 1)
coeff = 0.02


# do you want to show the run identifier in the terminal ouput and figures?
showid = False


