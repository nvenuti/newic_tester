import sys
import numpy as np
import math as m
from numba import jit, njit, float64, prange



import globalvars as glb
import funk as f

nu = glb.nu
npoints = glb.npoints
pa_rstop = glb.pa_rstop
ecc0 = glb.ecc0

#-------------------------------------------------------------------------------
# PA stuff

def check_shape(orbit_shape):
    if orbit_shape != 'circular':
        print('Cannot use PA approx. in non-circular case')
        sys.exit()
    else:
        return

# 2PA initial conditions for quasi circular motion
# and for solving dynamics with r grid
@njit
def circ_2pa(r0, j0, order='1'):

    
    # pi1 = E/A^2 * F_phi * dj/dr^-1
    # 1PA
    pi1 = -64/5 * nu * (r0-3)/(r0-6) /(r0-2) /r0**2

    if order == '1':
        #Update also E with PA espressions
        E = m.sqrt(pi1**2 * f.a(r0)**2 + f.a(r0) * ((j0/r0)**2 + 1))

        return  True, pi1, j0, E

    #2PA
    dpi1_dr = 64/5 * nu * ( 1/( (r0-2)**2 * (r0-6) * r0**3) + 2*(r0-3)*(r0-4) / ( (r0-2)**2 * (r0-6)**2 * r0**2 ) )

    k2 = r0**2 * ( f.a(r0)**2 *pi1 * dpi1_dr + f.a(r0) * pi1**2 *2/r0**2 ) #nb last term is A'(r)

    p_phi = j0 * m.sqrt(1+k2)
    
    # 3PA TODO
#    pi3 = 
     
    #Update also E with PA espressions
    E = m.sqrt(pi1**2 * f.a(r0)**2 + f.a(r0) * ((p_phi/r0)**2 + 1))


    return  True, pi1, p_phi, E

# 2PA initial conditions for quasi circular motion
# @njit
def elli_2pa(p0, ecc0, order='1'):

    # initial position ar apastron
    r0 = elli_r0(p0, ecc0)

    #adiabatic angular momentum
    j0 = elli_j0(p0, ecc0)

    #analytic expression for derivative of j0
    dj0_dr = elli_dj0dr(p0, ecc0)
    
    # pi1 = E/A^2 * F_phi * dj/dr^-1
    pi1 = elli_pi1(r0, ecc0)

    if order == '1':
        #Update also E with PA espressions
        E = m.sqrt(pi1**2 * f.a(r0)**2 + f.a(r0) * ((j0/r0)**2 + 1))

        return  True, pi1, j0, E

    # finite difference for derivative of pi1
    dr = 0.001
    dpi1_dr = ( 1/12 *elli_pi1(r0-2*dr, ecc0)
                - 2/3 *elli_pi1(r0-1*dr, ecc0)
                + 2/3 *elli_pi1(r0+1*dr, ecc0)
                -1/12 *elli_pi1(r0+2*dr, ecc0) 
                ) / dr


    k2 = r0**2 * ( f.a(r0)**2 *pi1 * dpi1_dr 
                    + f.a(r0) * pi1**2 *2/r0**2    #nb last term is A'(r)
                    + pi1 /r0**3 
                        * nu/2 * (21* (elli_j0(p0, ecc0)/r0)**2 - 1/r0) 
                        * m.sqrt(f.a(r0) *(elli_j0(p0, ecc0)/r0)**2 + 1) 
                    )
    # breakpoint()
    # p_phi = r0/m.sqrt(r0-3) * m.sqrt(1+k2)
    p_phi = r0/m.sqrt(r0+3) * m.sqrt(1+k2)


    E = m.sqrt(pi1**2 * f.a(r0)**2 + f.a(r0) * ((p_phi/r0)**2 + 1))


    return  True, pi1, p_phi, E


# everything defined as functions
# so finite differences are more practical

def elli_r0(p0, e0):
    return p0 / (1-e0)

def elli_j0(p0, e0):
    return  p0 / m.sqrt(p0 - 3 - e0**2)


#analytic expression for derivative of j0
def elli_dj0dr(p0, e0):
    # j0_ell^2 = j0_circ^2 * k_coeff(p, e)^2
    # j0 for elliptic orbit is smaller than for a circular orbit
    # because we start at apastron
    r0 = p0/(1-e0)
    k_coeff = m.sqrt(1 - e0*(1 + (e0-1)*(e0+3)/(p0-3-e0**2) ) )

    return 1/2 * ( (r0-6)/(r0-3)**1.5 * k_coeff
                    + r0/(r0-3)**0.5
                        * (e0*(e0-1)**2*(e0+3)) 
                        / (p0 -3 -e0**2) 
                        / k_coeff
                    )

def elli_pi1(r0, e0):
    p0 = r0*(1-e0)
    return -32/5 * nu * f.a(r0)/((elli_j0(p0, e0)/r0)**2 +1 )**2 * (elli_j0(p0, e0)/r0)**5 /r0 / elli_dj0dr(p0, e0)


#-------------------------------------------------------------------------------

# failed attempt at using a more precise F_phi in
# the first PA iteration
# unclear how to optain a usable espression
# flucts of pr still greater than usual PA
#@njit
#def my_pa(ins, orbit_shape):
#    if orbit_shape != 'circ':
#        print('Cannot use PA approx. in non-circular case')
#        sys.exit()
#    else:
#        pass
#
#    r = ins[0]
#    
#    pi1 = -64/5 * nu * (r-3)/(r-6) /(r-2) /r**2
#    
#    pi2 = m.sqrt(a(r)*(r-2)/(r-3)) * tri_dp_phi_dt(r, pi1, ins[3], ins[4]) / ( a(r)**2 *(r-6) /2 /(r-3)**(1.5) )
#
#    # print(dp_phi_dt(r, ins[3], ins[4]) , full_dp_phi_dt(r, pi1, ins[3], ins[4]))
#    # print(pi1, pi2)
#
#    return  True, pi2



# resolution of dynamics with 2PA approx on r_grid

@njit
def circ_pa_solve(init, npoints, grid):

    boolfiller, grid[0, 2], grid[0, 3], grid[0, 4] = circ_2pa(init[0], grid[i, 3])

    progress_update = npoints/10
    
    delta_r = grid[1,0] - grid[0, 0]
    
    for i in range(1, npoints):
        if i % progress_update == 0:
            print(i, '/', npoints, ' points')

        boolfiller, grid[i, 2], grid[i, 3], grid[i, 4] = circ_2pa(grid[i, 0], grid[i, 3])

        grid[i, 1] =   (grid[i-1, 1]
                        + delta_r
                            * ( grid[i, 3]/(grid[i, 2] *f.a(grid[i, 0]) *grid[i, 0]**2) 
                                + grid[i-1, 3]/(grid[i-1, 2] *f.a(grid[i-1, 0]) *grid[i-1, 0]**2) 
                               ) /2
                        )
                        
        grid[i, 5] =   (grid[i-1, 5]
                        + delta_r
                            * ( grid[i, 4]/(grid[i, 2] *f.a(grid[i, 0])**2)
                                + grid[i-1, 4]/(grid[i-1, 2] *f.a(grid[i-1, 0])**2) 
                               ) /2
                       )
    # if true, then we surely haven't stopped at the manual threshold pa_rstop
    # but at the same radius where the numerical solve stopped
    # so it makes sense to compare final times with numerical solve
    if grid[npoints-1, 0] != pa_rstop:
        print('final t of pa solve : ', grid[npoints-1, 5])

    
    if np.isnan(np.dot(grid[:, 5], grid[:, 5])):
        print('Nan found integrating times, what\'s going on?')
        sys.exit()
    
    if np.isnan(np.dot(grid[:, 1], grid[:, 1])):
        print('Nan found integrating phases, what\'s going on?')
        sys.exit()
        
    return True, grid  
