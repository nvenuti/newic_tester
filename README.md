# newic_tester #

Python code for a didactic approach to the EOB formalism for binary compact system.

Part of the work for my master thesis. The purpose of this code is to familiarize myself with the paradigm of the current EOB models (chiefly TEOBResumS)
and test the improvements that I'll study for my master thesis.

Pending : detailed features of the code

## Non-standard package requirements ##
numpy

matplotlib.plotly

numba

scipy.integrate

## Running ##

'main.py' : integrate equation of motion of the system and save results.

'graph.py' : generate different types of plot implemented

'testpa.py' : compare circular and elliptic 2PA results for different initial values

Author: Nicolò Venuti